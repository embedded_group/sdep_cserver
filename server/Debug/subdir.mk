################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../json2sdep_lib.c \
../samplingBuffer.c \
../sdep2json.c \
../server.c \
../server_f.c \
../server_testing.c 

OBJS += \
./json2sdep_lib.o \
./samplingBuffer.o \
./sdep2json.o \
./server.o \
./server_f.o \
./server_testing.o 

C_DEPS += \
./json2sdep_lib.d \
./samplingBuffer.d \
./sdep2json.d \
./server.d \
./server_f.d \
./server_testing.d 


# Each subdirectory must supply rules for building sources it contributes
%.o: ../%.c
	@echo 'Building file: $<'
	@echo 'Invoking: GCC C Compiler'
	gcc -std=c99 -I/usr/local/include/json-c -I"/root/git/sdep_cserver/server/sdepLibrary/include" -I"/root/git/sdep_cserver/server/sdepPHYLibrary" -O0 -g3 -Wall -c -fmessage-length=0 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


