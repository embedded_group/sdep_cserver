/*
 * anodine-timeManager.h
 *
 *  Created on: 17 lug 2016
 *      Author: anodine
 */

#ifndef ANODINE_TIMEMANAGER_H_
#define ANODINE_TIMEMANAGER_H_

#include <time.h>
#include <linux/time.h>
#include <unistd.h>

typedef enum {SEC,MILLI,MICRO,NANO} timeUnit;


static long int start_time;
static long int current_time;


int anodineSleep(timeUnit unit, long int timeValue);

//returns 0 on success, -1 on errors
int anodine_timeManager_startTimer();

long int anodine_timeManager_getTimer(timeUnit unit);


#endif /* ANODINE_TIMEMANAGER_H_ */
