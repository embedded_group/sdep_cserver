/**
* University of Naples Federico II
* Master of Embedded System
*
* Academic Year: 2015-2016
*
* Group Number: 2
* Engineers: Castello Oscar
*            Iorio Raffaele
*/

#ifndef SDEP_MESSAGE_H
#define SDEP_MESSAGE_H

#include "sdepConfig.h"

/* SDEP Protocol Define */
#define NULL_mType			0x00
#define	WHO_mType			0x01
#define	IAM_mType			0x02
#define	YOUARE_mType		0x03
#define	ACK_mType			0x04
#define	AREYOUALIVE_mType	0x05
#define ALIVEACK_mType		0x06
#define	BYEBYE_mType		0x07
#define	WHAT_mType			0x08
#define	MYCAP_mType			0x09
#define	CONF_mType			0x0A
#define	DREQ_mType			0x0B
#define	DEXG_mType			0x0C
#define	ERROR_mType			0x0D
#define	DREQIT_mType		0x0E
#define	DEXGIT_mType		0x0F
#define	ERRORIT_mType		0x10
#define	CUSTOM_mType		0x1F
#define mType_MASK			0x7F

#define mTypeSIZE			(sizeof(mType_t))
#define mTypeOFFSET			(mTypeSIZE)
#define mAddressSIZE		(sizeof(mAddress_t))
#define mAddressOFFSET		(mTypeSIZE+mAddressSIZE)

// If is set uSDEP in Slave mode
#if setMICROSDEP == 1 && setMASTER == 0
#define pLengthSIZE			(sizeof(pLength_t)+sizeof(pLength_t))
#else
#define pLengthSIZE			(sizeof(pLength_t))
#endif

#define pLengthOFFSET		(mTypeSIZE+mAddressSIZE+pLengthSIZE)
#define headerSIZE			(mTypeSIZE+mAddressSIZE+pLengthSIZE)

// Sizing definitions
typedef struct {
	uint8_t msgType:7;
	uint8_t	intEnabled:1;
}					mType_t;
typedef uint8_t		mAddress_t;

// If is set uSDEP in Slave mode
#if setMICROSDEP == 1 && setMASTER == 0
typedef uint8_t		pLength_t;
typedef uint8_t		pPadding_t;
#else
typedef uint16_t	pLength_t;
#endif

typedef uint8_t		mPayload_t;

typedef struct {
	mType_t		mType;
	mAddress_t	mAddress;

// If is set uSDEP in Slave mode
#if setMICROSDEP == 1 && setMASTER == 0
	pLength_t   pLength;
	pPadding_t  pPadding;
#else
	pLength_t	pLength;
#endif

	mPayload_t	*mPayload;
} sdepMessage_t;

#define pSIZE_MAX	(1<<(8*sizeof(pLength_t)))

sdepMessage_t *newMessage(mType_t mType, mAddress_t mAddress, pLength_t pLength, mPayload_t *mPayload);
void freeMessage(sdepMessage_t *sdepMessage);
uint32_t sendMessage(sdepMessage_t *sdepMessage);
uint32_t recvMessage(sdepMessage_t *sdepMessage);
#endif //SDEP_PROTOCOL_H
