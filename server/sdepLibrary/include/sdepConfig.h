/**
* University of Naples Federico II
* Master of Embedded System
*
* Academic Year: 2015-2016
*
* Group Number: 2
* Engineers: Castello Oscar
*            Iorio Raffaele
*/

#ifndef SDEP_CONFIG_H
#define SDEP_CONFIG_H

/**
 * Use this configuration for correct settings of the library.
 * Use setMASTER for select the Master or Slave version of the library.
 * If the Slave version is set, you can use uSDEP for device with small size RAM
 * by setting the useMICROSDEP to 1.
 * If you want, you can use or not the malloc operated version of the library
 * by setting the useMALLOC flag.
 */

// If is set the master function are enabled and the slave function are disabled
#define setMASTER	  1

// If is set the master function are enabled and the slave function are disabled
#define setMICROSDEP  0

// If is set the malloc function is used for the sdepMessage
#define useMALLOC	  1

#endif //SDEP_CONFIG_H
