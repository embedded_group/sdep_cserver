/**
* University of Naples Federico II
* Master of Embedded System
*
* Academic Year: 2015-2016
*
* Group Number: 2
* Engineers: Castello Oscar
*            Iorio Raffaele
*/

#ifndef SDEP_PROTOCOL_H
#define SDEP_PROTOCOL_H

#include "sdepConfig.h"

// SDEP Request Type configs
#define ONESHOTMode		0b00
#define THRESHOLDMode	0b01
#define PERIODICMode	0b10

typedef struct {
	uint8_t sensorType;		    //STYP
	uint8_t sensorNum;		    //SNUM
} sId_t;

typedef struct {
	sId_t	sensorId;		    //SID
	uint8_t	errorCode;		    //ECOD
} sError_t;

typedef struct {
	uint8_t	sensorType;		    //STYP
	uint8_t	sensorQuantity;	    //SQTY
	struct {
		uint8_t sampleLen:4;    //Length of one sample in byte
		uint8_t reqSupport:2;	//00-OneShot, 01-Threshold, 10-Periodic
		uint8_t intSupport:1;   //0-Interrupt not supported, 1-Interrupt supported
		uint8_t reserved:1;
	} sensorCap;		        //SCAP
} sCapability_t;

typedef struct {
	sId_t	sensorId;		    //SID
	uint8_t	*samplesPacket;	    //N0, N1, N2, ...
} sExchange_t;

typedef struct {
	sId_t	sensorId;           //SID
	uint8_t intType;		    //ITYP
	uint8_t	*samplesPacket;     //N0, N1, N2, ...
} sExchangeInt_t;

typedef struct {
	sId_t	sensorId;           //SID
	struct {
		uint8_t sActive:1;      //0-OFF, 1-ON
		uint8_t reqSettings:2;  //00-OneShot, 01-Threshold, 10-Periodic
		uint8_t intSetting:1;   //0-Disable interrupt, 1-Enable interrupt
		uint8_t reserved:4;
	} sensorMode;               //MODE
	uint8_t sensorConfig;       //CONFIG
} sConfig_t;

#if setMASTER == 0
// -----------------------------
// ------ Slave Functions ------
// -----------------------------

int32_t sendAck();
int32_t sendError(sError_t sErrorList[], uint16_t seNum);
int32_t sendErrorInt();
int32_t sendCapability(sCapability_t sCapsList[], uint16_t scNum);
int32_t sendSamplesPacket(sId_t sensorId, uint8_t samplesArray[], uint16_t arrSize);
int32_t sendSamplesPacketInt(sId_t sensorId, uint8_t intType, uint8_t samplesArray[], uint16_t arrSize);
int32_t sendCustom(uint8_t *mPayload, uint16_t pLength);

#else
// -----------------------------
// ------ Master Functions -----
// -----------------------------

int8_t sendDisconnect(uint8_t pAddress);
int8_t sendCapabilityRequest(uint8_t pAddress);
int8_t sendConfigTo(uint8_t pAddress, sConfig_t sConfList[], uint16_t scNum);
int8_t sendSamplesRequest(uint8_t pAddress, sId_t sensorId);
int8_t sendSamplesRequestInt(uint8_t pAddress);
int8_t sendCustom(uint8_t pAddress, uint8_t *mPayload, uint16_t pLength);

#endif //setMASTER

#endif //SDEP_PROTOCOL_H
