#include "server_f.h"
#include <sys/stat.h>
#include <fcntl.h>
#include "json2sdep_lib.h"

#define DEBUG //enables printf debug
//#define TESTING

extern json_object *jresp;

/***********SDEP DATA STRUCTURES******************/
extern uint32_t attachedNum;
extern sdepAddressList_f *addressList;
/*******************END***************************/

/****************SIGNAL FLAG**********************/
extern int isSignaled;
/*********************END*************************/

/****************CLIENT INFO**********************/
struct sockaddr client;
/********************END**************************/

/**************THRESHOLD PORT INFO****************/
extern struct sockaddr_in server_addr;
extern struct hostent *hp;
/**********************END************************/

/*************SERVER SOCKET DESCRIPTORS***********/
extern	int listenfd;
extern int connfd;
/**************************END********************/

/***** BEGIN PERIODIC ELABORATION DATAS***********/
extern uint8_t data_oneshot[INDEX_ONESHOT];
extern uint8_t current_index_oneshot;
/*************************END*********************/

int main()
{
	char buff[BUF_SZ];
	bzero(buff,BUF_SZ);
	buff[0] = '\0';
	int expr = -1;
	uint8_t pAddress = 0;

	json_object *temp = NULL;
	int32_t i,retVal;


#ifndef TESTING
	/***********************Inizializza stm *************************************/

	// Initialize the callbacks
	phyDriver_t phyDriver = {
			.selectAddress = selectAddress,
			.writeBytes = writeChars,
			.readBytes = readChars
	};

	sdepCallbacks_t sdepCallbacks = {
			.ackResponse = ackResponse,
			.errorResponse = errorResponse,
			.errorResponseInt = errorResponseInt,
			.capabilityResponse = capabilityResponse,
			.dataArriveResponse = dataArriveResponse,
			.dataArriveResponseInt = dataArriveResponseInt,
			.customResponse = customResponse
	};

	retVal = openDriver();
	if (retVal != SUCCESS){
		if (retVal == -EPERM) {
			printf("Error opening /dev/sdepUSB file driver. Check your permissions.\n");
		}
		if (retVal == -EBUSY) {
			printf("Error opening /dev/sdepUSB file driver. Already in use.\n");
		}
		return -1;
	}

	if(getAttachedList(&addressList,&attachedNum) == SUCCESS){
		printf("Attached device list:\n");
		for (i=0; i<attachedNum; i++){
			printf("SDEP Device[%d] at address %d with serial %08x\n",i, addressList[i].pAddress, addressList[i].pSerial);
		}
	}

	registerSignalHandler(SIGLISTCHANGE, sig_handler);
	registerSignalHandler(SIGDEVINTERRUPT, sig_handler);

	// Initialize the SDEP core
	if(sdepInitCore(phyDriver, sdepCallbacks)!= sdepOK){
		printf("[LOG] Errore init core.\n");
		return -1;
	}
	printf("[LOG] SdepCore inizializzato.\n");

#endif

	server_init(&listenfd);

	/**************INIT THRESHOLD PORT***********************************/
	hp = gethostbyname("127.0.0.1");

	server_addr.sin_family = AF_INET;
	server_addr.sin_port = htons(THRESHOLD_PORT);
	bcopy((char *)hp->h_addr_list[0], (char *)&server_addr.sin_addr.s_addr, hp->h_length);
	/************************END*****************************************/

	if(listen(listenfd, 1) < 0)
		printf("Some error in listening\n");

	while(1)
	{
		printf("[LOG] Listening.\n");
		//Open connfd
		isSignaled = 0;

		connfd = accept(listenfd, (struct sockaddr*)NULL, NULL);

		if(isSignaled == 1)
		{
			printf("[LOG] Signal message received while listening.\n");
			isSignaled = 0;
			goto server_endloop;
		}

		jresp = json_object_new_object();

		bzero(buff,BUF_SZ);
		buff[0] = '\0';
		readString(connfd,buff);

		printf("[LOG] Received json object: %s (line %d).\n", buff, __LINE__);

		json_object * jobj = json_tokener_parse(buff);

		printf("[LOG] Parsing JSON string...\n");
		//@ gp 13/07/2016 aggiunta logica per prevenire errori di parsing
		if( checkParser(jobj,connfd) < 0 )
			goto server_closeloop;

		printf("[LOG] Parsing successful.\n");

		//@rm cerca il campo metodo nella stringa json e ne estrae il valore
		if(findMethod(jobj,&expr) < 0){
			printf("[LOG] Method field not found in json string.\n");
			ADD_RET(ERROR_METHOD);
			sendJSONString(connfd);
			goto server_closeloop;
		}

#ifndef TESTING

		//@rm se il metodo è diverso da G_ATT_LIST (che è l'unico che non ha bisogno di pAddress) cerca pAddress nella json string
		//e ne estrae il valore
		if(expr != G_ATT_LIST){
			if(findAddress(jobj,&pAddress) < 0)
			{
				printf("[LOG] Errore pAddress field not found in json string.\n");
				ADD_RET(ERROR_PADDRESS);
				sendJSONString(connfd);
				goto server_closeloop;
			} else if(checkAddressExistence(pAddress,addressList, attachedNum) < 0 ) //@rm verifica che la probe indirizzata sia collegata
			{
				printf("[LOG] Non esiste questa probe.\n");
				ADD_RET(ERROR_NOT_VALID_ADDRESS);
				sendJSONString(connfd);
				goto server_closeloop;
			}
		}
#endif
		//serve perchè altrimenti all'interno del while il vecchio oggetto viene sovrascritto e si perde memoria
		json_object_put(jresp);

#ifdef TESTING
		switch (expr) {
		case G_ATT_LIST:
			printf("[LOG] GetAttachedList called!\n");
			//	sendJSONString(connfd);
			testAttached();

			json_object_object_add(jresp,"return",json_object_new_string("sdepOK"));
			sendJSONString(connfd);
			break;
		case C_ALIVE:
			printf("[LOG] checkAlive called!\n");
			//	testCheckAlive();

			break;
		case S_DISCONNECT:
			printf("sendDisconnect\n");
			json_object_object_add(jresp,"return",json_object_new_string("sdepOK"));

			testDisconnect(pAddress);
			//	sendOKString(connfd);
			break;
		case S_CAPABILITY:
			printf("[LOG] sendCapabilityRequest.(line %d)\n", __LINE__);
			testCapability();
			json_object_object_add(jresp,"return",json_object_new_string("sdepOK"));
			sendJSONString(connfd);
			break;
		case S_CONFIG:
			printf("config\n");
			testConfig(jobj);
			printf("fuori dal test\n");
			jresp = json_object_new_object();
			json_object_object_add(jresp,"return",json_object_new_string("sdepOK"));
			sendJSONString(connfd);

			break;
		case S_SAMPLE_REQ:
			if(manager_data_parsejson(jresp, connfd,jobj,&infos) < 0)
			{
				printf("errore (gp : devi aggiungere info ulteriori)");
			}

			manager_data_acquisition_start(&infos,connfd);

			//ADD_RET("sdepOK");
			//sendJSONString(connfd);




			//					/****call anodine func ***/
			//					printf("Hai ricevuto dal client le seguenti info: \n"\
			//
			//
			//
			//
			//					"sampleInterval:%d, timePeriod:%d,pAddr:%d,sid_num:%d, sid_type:%d, samLen:%d	\n",
			//					sampleInterval,timePeriod,pAddr, sidToSample.sensorNum, sidToSample.sensorType,samLen);
			//
			//
			//
			//
			//
			//					ADD_RET("sdepOK");
			//					sendJSONString(connfd);
			//
			//
			//
			//			//           	    	sId_t *sensorId;
			//			//           	    	jSon2sId_t(jobj, &sensorId);
			//			//           	    	sendSamplesRequest(pAddress, sensorId);
			//
			//			json_object_put(oggInterval);
			//			json_object_put(oggPAddr);
			//			json_object_put(oggPeriod);
			//			json_object_put(oggSampleLen);
			//			json_object_put(oggSamplingRequest);
			//			json_object_put(oggStringSampling);
			//			json_object_put(oggSid);




			break;
		case CLOSE:
			printf("CHIUSURA DEL SERVER\n");
			close(connfd);
			return EXIT_SUCCESS;
			break;
		}

#else

		switch (expr) {
		case G_ATT_LIST:
			printf("[LOG] getAttachedList CASE.\n");
			jresp = addrList_Json(addressList, attachedNum);
			ADD_RET(SDEP_OK);
			sendJSONString(connfd);
			break;
		case S_DISCONNECT:
			printf("[LOG] forceDisconnect CASE.\n");
			jresp = json_object_new_object();
			ADD_RET("sdepOK");
			callSendDisconnect(pAddress);
			sendJSONString(connfd);
			break;
		case S_CAPABILITY:
			printf("[LOG] sendCapabilityRequest CASE.\n");
			callSendCapabilityRequest(pAddress);
			sendJSONString(connfd);
			break;
		case S_CONFIG:
			printf("[LOG] ConfigTo CASE.\n");
			jresp = json_object_new_object();

			temp = json_object_object_get(jobj,JSON_KEY_VARIABLE_FIELD);

			if (temp == NULL)
				printf("[LOG ]ERROR: field scNum not found!\n");

			uint16_t scNum = json_object_get_int(temp);

#ifdef DEBUG
			printf("[LOG] scNum: %d.\n",scNum);
#endif

			sConfig_t *configList=(sConfig_t*)malloc(sizeof(sConfig_t)*scNum);

#ifdef DEBUG
			printf("[LOG] Malloc of configList done.\n");
#endif
			json_object *oggconf = json_object_object_get(jobj, "object");

#ifdef DEBUG
			printf("[LOG] configList json: %s.\n",json_object_get_string(oggconf));
			printf("[LOG] Parsing...\n");
#endif
			json_object * oggconfparse = json_tokener_parse(json_object_get_string(oggconf));

#ifdef DEBUG
			printf("[LOG] Parsed...\n");
#endif

			if(checkParser(oggconfparse,connfd) < 0)
			{
				printf("[LOG] error parsing json object della chiave object in S_CONFIG\n");
				goto server_closeloop;
			}
#ifdef DEBUG
			printf("[LOG] Converting json configList string to data structure.\n");
#endif

			jSon2sConfig_tList(oggconfparse, configList);
			printf("[LOG] Converted.\n");
			json_object_put(oggconfparse);


#ifdef DEBUG

			for(int i=0; i<scNum; i++){
				printf("[LOG] configList[%d]:\n sensorId.sensorType = %d, sensorId.sensorNum = %d,sensorMode.sActive = %d, sensorMode.reqSettings = %d, sensorMode.reserved = %d\nsensorConfig = %d\n",i,
						configList[i].sensorId.sensorType,configList[i].sensorId.sensorNum,configList[i].sensorMode.sActive,configList[i].sensorMode.reqSettings,configList[i].sensorMode.reserved,configList[i].sensorConfig);
			}

			printf("[LOG] Calling sendConfigTo.\n");
			printf("[LOG] pAddress: %d.\n",pAddress);
			printf("[LOG] scNum: %d.\n",scNum);
#endif

			callSendConfigTo(pAddress, configList, scNum);
			printf("[LOG] sendConfigTo called.\n");
			sendJSONString(connfd);
			free(configList);
			break;

		case PERIODIC_REQ:
			jresp = json_object_new_object();
			printf("[LOG] PERIODIC REQ CASE.\n");
			DATA_REQUEST_MODE = PERIODICMode;

			temp = json_object_object_get(jobj,JSON_KEY_VARIABLE_FIELD);

			if (temp == NULL)
				printf("[LOG ]ERROR: field scNum not found!\n");

			scNum = json_object_get_int(temp);

#ifdef DEBUG
			printf("[LOG] scNum: %d.\n",scNum);
#endif

			configList=(sConfig_t*)malloc(sizeof(sConfig_t)*scNum);

#ifdef DEBUG
			printf("[LOG] Malloc of configList done.\n");
#endif
			oggconf = json_object_object_get(jobj, "object");

#ifdef DEBUG
			printf("[LOG] configList json: %s.\n",json_object_get_string(oggconf));
			printf("[LOG] Parsing...\n");
#endif
			oggconfparse = json_tokener_parse(json_object_get_string(oggconf));

#ifdef DEBUG
			printf("[LOG] Parsed...\n");
#endif

			if(checkParser(oggconfparse,connfd) < 0)
			{
				printf("[LOG] error parsing json object della chiave object in S_CONFIG\n");
				goto server_closeloop;
			}
#ifdef DEBUG
			printf("[LOG] Converting json configList string to data structure.\n");
#endif

			jSon2sConfig_tList(oggconfparse, configList);
			printf("[LOG] Converted.\n");
			json_object_put(oggconfparse);


#ifdef DEBUG

			for(int i=0; i<scNum; i++){
				printf("[LOG] configList[%d]:\n sensorId.sensorType = %d, sensorId.sensorNum = %d,sensorMode.sActive = %d, sensorMode.reqSettings = %d, sensorMode.reserved = %d\nsensorConfig = %d\n",i,
						configList[i].sensorId.sensorType,configList[i].sensorId.sensorNum,configList[i].sensorMode.sActive,configList[i].sensorMode.reqSettings,configList[i].sensorMode.reserved,configList[i].sensorConfig);
			}


			printf("[LOG] pAddress: %d.\n",pAddress);
			printf("[LOG] scNum: %d.\n",scNum);
#endif
			json_object *SLobj = json_object_object_get(jobj,JSON_KEY_SAMPLING_INT );
			int SLen = json_object_get_int(SLobj);
			printf("sampling interval:%d\n",SLen);
			Periodic_elaboration(connfd,pAddress,configList,SLen,scNum);

			free(configList);
		break;

		case ONESHOT_REQ:
			printf("[LOG] Oneshot CASE.\n");
			jresp = json_object_new_object();
			json_object *varfield = json_object_object_get(jobj,JSON_KEY_VARIABLE_FIELD);

#ifdef DEBUG
			printf("[LOG] varField: %s.\n",json_object_get_string(varfield));
#endif

			json_object *sid = json_tokener_parse(json_object_get_string(varfield));

			if(checkParser(sid,connfd) < 0)
			{
				printf("[LOG] error parsing json object della chiave variableField in S_SAMPLE_REQ\n");
				goto server_closeloop;
			}

			sId_t sId;

			if( jSon2sId_t(sid,&sId) == 0)
				printf("[LOG] Conversion ok.\n");

#ifdef DEBUG
			printf("sId_t.sensorType = %d, sId_t.sensorNum = %d.\n",sId.sensorType,sId.sensorNum);
#endif
			DATA_REQUEST_MODE = ONESHOTMode;

			char* ret = assertCall(sendSamplesRequest(pAddress, sId));

			printf("[LOG] sendSamplesRequest Called!\n");

			printf("addertCall:%s\n",ret);

			if(strcmp(ret,"sdepOK") != 0)
			{
				printf("[LOG] sendSamplesRequest Failed. Return value: %s.\n",ret);
				ADD_RET(ret);
				sendJSONString(connfd);
				break;
			}

			printf("data to send :\n");

			for(int i = 0; i < current_index_oneshot; i++)
				printf("%d ",data_oneshot[i]);
			printf("\ncurrent_index: %d\n",current_index_oneshot);

			sExchange_t exchangeData ;
			exchangeData.samplesPacket = data_oneshot;
			exchangeData.sensorId = sId;

			json_object *obj_data = sExchange_tJson(&exchangeData,current_index_oneshot);
			json_object_object_add(jresp, JSON_KEY_OBJECT,obj_data);
			ADD_RET(ret);
			current_index_oneshot = 0;
			sendJSONString(connfd);
			break;
		case CLOSE:
			printf("[LOG] Chiusura del server.\n");
			close(connfd);
			return EXIT_SUCCESS;
		default:
			printf("[LOG] Metodo non riconosciuto.\n");
			break;
		}

#endif
		server_closeloop:

		//@gp jresp va distrutto ogni volta che si fa il loop !
		if (json_object_put(jresp) == 1)
			printf("[LOG] jresp liberato.\n");
		else printf("[LOG] jresp non liberato.\n");

		if(json_object_put(jobj) == 1)
			printf("[LOG] jobj liberato.\n");
		else printf("[LOG] jobj non liberato.\n");

		server_endloop:

		//Close connfd
		close(connfd);
	}

	printf("FINE: %s\n", buff);

	//Driver attivo
#ifndef TESTING
	closeDriver();
#endif
	return EXIT_SUCCESS;

}
