/*
 * sdep2json.c
 *
 *  Created on: 03 lug 2016
 *      Author: rmaisto
 */

#include "sdep2json.h"
#include "server_f.h"
#include <stdlib.h>
#include <string.h>

/**
 * @brief Converts a sId_t struct into a JSON object and returns it
 * @param sensorId - pointer to the struct to convert
 * @retval pointer to JSON object
 */
json_object* sId_tJson(sId_t* sensorId)
{
	json_object *jobj = json_object_new_object();
	json_object *jsensType = json_object_new_int(sensorId->sensorType);
	json_object *jsensNum = json_object_new_int(sensorId->sensorNum);
	json_object_object_add(jobj,JSON_KEY_SENSOR_TYPE,jsensType);
	json_object_object_add(jobj,JSON_KEY_SENSOR_NUM,jsensNum);
	return jobj;
}

/**
 * @brief Converts a sError_t struct into a JSON object returns it
 * @param error - pointer to the struct to convert
 * @retval pointer to JSON object
 */
json_object* sError_tJson(sError_t* error)
{
	json_object *jobj = json_object_new_object();
	json_object *jerrorCode = json_object_new_int(error->errorCode);
	json_object_object_add(jobj,JSON_KEY_SENSORID,sId_tJson(&(error->sensorId)));
	json_object_object_add(jobj,JSON_KEY_ERRORCODE,jerrorCode);
	return jobj;
}

/**
 * @brief Converts a sCapability_t struct into a JSON object returns it
 * @param cap - pointer to the struct to convert
 * @retval pointer to JSON object
 */
json_object* sCapability_tJson(sCapability_t* cap)
{
	json_object* jobj = json_object_new_object();
	json_object* jsensType = json_object_new_int(cap->sensorType);
	json_object* jsensQuant = json_object_new_int(cap->sensorQuantity);

	uint8_t sensorCap = cap->sensorCap.reserved | cap->sensorCap.intSupport << 1 | cap->sensorCap.reqSupport << 2 | cap->sensorCap.sampleLen << 4;

	json_object* jsensCap = json_object_new_int(sensorCap);
	json_object_object_add(jobj,JSON_KEY_SENSOR_TYPE,jsensType);
	json_object_object_add(jobj,JSON_KEY_SENSOR_QUANTITY,jsensQuant);
	json_object_object_add(jobj,JSON_KEY_SENSOR_CAP,jsensCap);
	return jobj;
}

/**
 * @brief Converts a sExchange_t struct into a JSON object and returns it
 * @param exchange - pointer to the struct to convert
 * @param arrSize - dimension of the sExchange.samplesPacket array field
 * @retval pointer to JSON object
 */
json_object* sExchange_tJson(sExchange_t* exchange, uint16_t arrSize)
{
	int i;
	json_object* jobj = json_object_new_object();
	json_object* jsamArr = json_object_new_array();

	for(i = 0; i < arrSize; i++){
		json_object* jint = json_object_new_int(exchange->samplesPacket[i]);
		json_object_array_add(jsamArr,jint);
	}
	json_object_object_add(jobj,"sensorId",sId_tJson(&(exchange->sensorId)));
	json_object_object_add(jobj,"samplesPacket",jsamArr);
	return jobj;
}

/**
 * @brief Converts a sExchangeInt_t struct into a JSON object and returns it
 * @param exchange - pointer to the struct to convert
 * @param arrSize - dimension of the sExchange.samplesPacket array field
 * @retval pointer to JSON object
 */
json_object* sExchangeInt_tJson(sExchangeInt_t* exchange, uint16_t arrSize)
{
	int i;
	json_object* jobj = json_object_new_object();
	json_object* jint = json_object_new_int(exchange->intType);
	json_object* jsamArr = json_object_new_array();

	for(i = 0; i < arrSize; i++){
		json_object* jint = json_object_new_int(exchange->samplesPacket[i]);
		json_object_array_add(jsamArr,jint);
	}
	json_object_object_add(jobj,"sensorId",sId_tJson(&(exchange->sensorId)));
	json_object_object_add(jobj,"intType",jint);
	json_object_object_add(jobj,"samplesPacket",jsamArr);
	return jobj;
}

/**
 * @brief Converts a sConfig_t struct into a JSON object and returns it
 * @param config - pointer to the struct to convert
 * @retval pointer to JSON object
 */
json_object* sConfig_tJson(sConfig_t* config)
{
	json_object* jobj = json_object_new_object();

	uint8_t sensorMode = config->sensorMode.reserved | config->sensorMode.reqSettings << 5 | config->sensorMode.sActive << 7;

	json_object* jsensMod = json_object_new_int(sensorMode);
	json_object* jsensConfig = json_object_new_int(config->sensorConfig);

	json_object_object_add(jobj,JSON_KEY_SENSORID,sId_tJson(&(config->sensorId)));
	json_object_object_add(jobj,JSON_KEY_SENSOR_MODE,jsensMod);
	json_object_object_add(jobj,JSON_KEY_SENSORCONFIG,jsensConfig);

	return jobj;
}

/**
 * @brief Converts a sdepAddressList_f struct into a JSON object and returns it
 * @param address - pointer to the struct to convert
 * @retval pointer to JSON object
 */
json_object* sAddress_fJson(sdepAddressList_f* address)
{
	json_object* jobj = json_object_new_object();
	json_object* jAddr = json_object_new_int(address->pAddress);
	json_object* jSerial = json_object_new_int(address->pSerial);

	json_object_object_add(jobj,JSON_KEY_PADDR,jAddr);
	json_object_object_add(jobj,JSON_KEY_PSERIAL,jSerial);

	return jobj;
}

/**
 * @brief Converts an array of sCapability_t struct into a JSON object and returns the string of it
 * @param capList - pointer to the array struct to convert
 * @param arrSize - dimension of the array
 * @retval pointer to JSON string
 */
json_object* capList_Json(sCapability_t* capList, uint16_t arrSize)
{
	int i;
	json_object* jobj = json_object_new_object();
	json_object* jArr = json_object_new_array();
	for(i = 0; i < arrSize; i++)
		json_object_array_add(jArr,sCapability_tJson(&capList[i]));
	json_object_object_add(jobj,JSON_KEY_OBJECT,jArr);
	return jobj;
}

/**
 * @brief Converts a sError_t struct array into a JSON object and returns the string of it
 * @param errList - pointer to the struct array to convert
 * @param arrSize - array dimension
 * @retval pointer to JSON string
 */
json_object* errList_Json(sError_t* errList,uint16_t arrSize)
{
	int i;
	json_object* jobj = json_object_new_object();
	json_object* jArr = json_object_new_array();
	for( i = 0; i < arrSize; i++)
		json_object_array_add(jArr,sError_tJson(&errList[i]));
	json_object_object_add(jobj,JSON_KEY_OBJECT,jArr);
	return jobj;
}

/**
 * @brief Converts a sConfig_t struct array into a JSON object and returns the string of it
 * @param confList - pointer to the struct array to convert
 * @param arrSize - array dimension
 * @retval pointer to JSON string
 */
json_object* confList_Json(sConfig_t* confList,uint16_t arrSize)
{
	int i;
	json_object* jobj = json_object_new_object();
	json_object* jArr = json_object_new_array();
	for(i = 0; i < arrSize; i++)
		json_object_array_add(jArr,sConfig_tJson(&confList[i]));
	json_object_object_add(jobj,JSON_KEY_OBJECT,jArr);
	return jobj;
}

/**
 * @brief Converts a sdepAddressList_f struct array into a JSON object and returns the string of it
 * @param addrList - pointer to the struct array to convert
 * @param arrSize - array dimension
 * @retval pointer to JSON srrtring
 */
json_object* addrList_Json(sdepAddressList_f* addrList, uint16_t arrSize){
	int i;
	json_object* jobj = json_object_new_object();
	json_object* jArr = json_object_new_array();
	for(i = 0; i < arrSize; i++)
		json_object_array_add(jArr,sAddress_fJson(&addrList[i]));
	json_object_object_add(jobj,JSON_KEY_OBJECT,jArr);
	return jobj;
}
