/*
 * json2sdep_lib.h
 *
 *  Created on: 04 lug 2016
 *      Author: anodine
 */

#ifndef JSON2SDEP_LIB_H_
#define JSON2SDEP_LIB_H_

#include <json-c/json.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include "sdep2json.h"

//MACROS
#define FRM_ERR -1 /*framing error*/
#define NULL_ERR -2 /*json object field's not found*/
#define OK 0

//Conversion methods
int jSon2sCapability_t(json_object *j, sCapability_t *capabilities);
int jSon2sId_t(json_object *j, sId_t *sensorId);
int jSon2sCapability_tList(json_object *j, sCapability_t *capList);
int jSon2sdepAddressList_f(json_object *j, sdepAddressList_f *addr);
int jSon2sdepAddressList_fList(json_object *j, sdepAddressList_f *addrList);
int jSon2sConfig_t(json_object *j, sConfig_t *config);
int jSon2sConfig_tList(json_object *j, sConfig_t *configList);
int jSon2sError_t(json_object *j, sError_t *error);
int jSon2sError_tList(json_object *j, sError_t *errList);

#endif /* JSON2SDEP_LIB_H_ */
