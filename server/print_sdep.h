/*
 * print_sdep.h
 *
 *  Created on: 14 lug 2016
 *      Author: root
 */

#ifndef PRINT_SDEP_H_
#define PRINT_SDEP_H_

#include "sdepProtocol.h"
#include "sdepMessage.h"
#include "sdepCore.h"
#include "sdep2json.h"

void sdep_print_sConfig(sConfig_t sconfig);

void sdep_print_sid(sId_t sid);



#endif /* PRINT_SDEP_H_ */
