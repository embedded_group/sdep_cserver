#ifndef __SERVER_F_H
#define __SERVER_F_H

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>
#include <strings.h>
#include <json-c/json.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netdb.h>
#include <arpa/inet.h>
#include <linux/time.h>
#include "sdepProtocol.h"
#include "sdepMessage.h"
#include "sdepCore.h"
#include "sdep2json.h"
#include "sdepPHY.h"
//#include "anodine-timeManager.h"
#include <time.h>
#define C_ALIVE 0
#define S_DISCONNECT 1
#define S_CAPABILITY 2
#define S_CONFIG 3
//#define S_SAMPLE_REQ 4
#define G_ATT_LIST 5
#define ONESHOT_REQ 6
#define PERIODIC_REQ 7
#define CLOSE 8

#define ERR_NOPROBES 0
#define ERR_NOOPENDRIVER 1
#define NO_ERR     2 
#define ERR_DRIVERSTM     3

#define BUF_DATA_SZ 8192
#define BUF_SZ 1024
#define ACK_OK "ACK_OK"
#define SDEP_OK "sdepOK"
#define THRESHOLD_PORT 7099

#define ERROR_JSON_PARSING "Error parsing"
#define ERROR_METHOD_OR_PADDRESS "Methodname or pAddress not found"
#define ERROR_METHOD "Method not found"
#define ERROR_PADDRESS "pAddress not found"
#define ERROR_NOT_VALID_ADDRESS "NOT VALID ADDRESS"
#define ERROR_NO_PERIODIC_SENSOR "This sensor is not periodic"
#define ERROR_PERIODIC_ELAB_ERR "Elaboration error"
#define PERIODIC_ERROR -1
#define TIMER_START_ERROR -2

#define JSON_KEY_SENSOR_MODE "sensorMode"
#define JSON_KEY_SENSOR_TYPE "sensorType"
#define JSON_KEY_SENSOR_NUM "sensorNum"
#define JSON_KEY_PADDR "pAddress"
#define JSON_KEY_SENSOR_CAP "sensorCap"
#define JSON_KEY_SENSOR_QUANTITY "sensorQuantity"
#define JSON_KEY_OBJECT "object"
#define JSON_KEY_PSERIAL "pSerial"
#define JSON_KEY_SENSORID "sensorId"
#define JSON_KEY_INTTYPE "intType"
#define JSON_KEY_SAMPLESPACKET "samplesPacket"
#define JSON_KEY_ERRORCODE "errorCode"
#define JSON_KEY_SENSORCONFIG "sensorConfig"
#define JSON_KEY_VARIABLE_FIELD "variableField"
#define JSON_KEY_SAMPLING_INT "samplingInt"
#define JSON_KEYS_DATA_END "END"
#define JSON_KEYS_DATA_RESPONSE "DR"
#define JSON_KEY_THRESHOLD_PORT "port"

/*** Request of sampling  ****/
#define JSON_KEY_SAMPLE_INTERVAL "sampleInterval"
#define JSON_KEY_TIME_SAMPLING  "timeSampling"


#define PORT_SERVER 6666

/*PERIODIC ELABORATION DEFINES*/
#define REQ_ONESHOT 1
#define REQ_PERIODIC 2

#define INDEX_ONESHOT 9
#define PERIODIC_DIM_BUFFER 1024

/***************************/

#define ADD_RET(ret) json_object_object_add(jresp,"return",json_object_new_string(ret));





//oggetto json globale contenente le risposte provenienti dalle callback
json_object *jresp;
json_object *jtresh;

// Protocol callbacks
int8_t ackResponse(mAddress_t pAddress);
int8_t errorResponse(mAddress_t pAddress, sError_t sErrorList[], uint16_t seNum);
int8_t errorResponseInt (mAddress_t pAddress);
//int8_t capabilityResponse(mAddress_t pAddress, sCapability_t sCapsList[], uint16_t scNum);
//int8_t dataArriveResponse(mAddress_t pAddress, sId_t sensorId, uint8_t samplesArray[], uint16_t arrSize);
int8_t customResponse(mAddress_t pAddress, mPayload_t *mPayload, pLength_t pLength);
int8_t dataArriveResponseInt (mAddress_t pAddress, sId_t sensorId, uint8_t intType, uint8_t samplesArray[], uint16_t arrSize);
void sig_handler(int sig_num);
int8_t capabilityResponse(mAddress_t pAddress, sCapability_t sCapsList[], uint16_t scNum);
//server functions
char* assertCall(int);
void callSendCapabilityRequest(mAddress_t);
void callSendConfigTo(uint8_t , sConfig_t* , uint16_t);
void callSendSamplesRequest(mAddress_t, sId_t );
void callSendDisconnect(mAddress_t);
void readString(int ,char* );
void server_init(int*);
int sendJSONString(int );
int sendDataJSONString(int );

int checkParser(json_object*, int);
int findMethod(json_object*,int*); //@rm
int findAddress(json_object*,uint8_t*); //@rm
int checkAddressExistence(uint8_t, sdepAddressList_f*, uint8_t);
int8_t dataArriveResponse(mAddress_t pAddress, sId_t sensorId, uint8_t samplesArray[], uint16_t arrSize);

int PeriodicEnd();
int Periodic_elaboration(int connfd,uint8_t pAddress, sConfig_t *  C_list, uint16_t SLen, uint16_t scNum);

typedef struct PERIODIC_BUFFER_T
{
	uint8_t pAddr;
	sId_t sid;
//	int port;
	uint8_t current_index;
	uint8_t buffer[PERIODIC_DIM_BUFFER];

} OnePeriodicBuffer;

typedef OnePeriodicBuffer * PeriodicBuffer;

//STUB PROTOTYPES
void testAttached();
void testCapability();
void testDisconnect(uint8_t);
void testDataSend(json_object*);
void testConfig(json_object*);
uint8_t server_extractObjectField(json_object* obj, json_object* object, json_object* variableObject);
uint8_t DATA_REQUEST_MODE;

#endif
