Per far funzionare il seguente progetto bisogna:
1) aggiungere alle proprietà del progetto le librerie quindi:
	- tasto DX sul progetto > properties > C/C++ Build > settings > GCC C Compiler > Includes in include paths cliccare su + poi su workspace ed 
	  aggiungere la cartella server/sdepLibrary/include
	- Come sopra ma aggiungere server/sdepPHYLibrary
2)	Aggiungere gli archivi e quindi: 
	- Tasto DX sul progetto > properties > C/C++ Build > settings > GCC C Linker > Miscellaneous in Other objects cliccare su + poi su workspace ed aggiungere
   	  server/sdepPHYLibrary/libsdepphy.a e server/sdepLibrary/libsdepmaster.a