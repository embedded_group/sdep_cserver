#define PERIODIC_ERROR -1
#define TIMER_START_ERROR -2

#include "sdepCore.h"
#include "anodine-timeManager.h"
#include <sys/socket.h>
#include <stdio.h>
#define PERIODIC_DIM_BUFFER 1024

//#define REQ_ONESHOT 1
//#define REQ_PERIODIC 2


void DataRequest_setOneshotMode();
void DataRequest_setPeriodicMode();





//typedef struct PERIODIC_BUFFER_T
//{
//	uint8_t pAddr;
//	sId_t sid;
////	int port;
//	uint8_t current_index;
//	uint8_t buffer[PERIODIC_DIM_BUFFER];
//} PeriodicBuffer;


int8_t dataArriveResponse(mAddress_t pAddress, sId_t sensorId, uint8_t samplesArray[], uint16_t arrSize);
/**
 * @brief Elaborazione di un campionamento periodico
 * @param pAddress - indirizzo della probe selezionata
 * @param C_list - lista delle configurazioni
 * @param SLen - intervallo di campionamento
 * @param scNum - numero di elementi della config list
 * @retval codici di errore PERIODIC_ERROR -1 , TIMER_START_ERROR -2
 */
//int Periodic_elaboration(int connfd,uint8_t pAddress, sConfig_t *  C_list, uint16_t SLen, uint16_t scNum);

//int PeriodicEnd();

//uint8_t Oneshot_Elaboration(int8_t pAddress, sId_t sId);
