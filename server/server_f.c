#include "server_f.h"
#include <sys/stat.h>
#include <fcntl.h>
#define DEBUG

//oggetto json globale contenente le risposte provenienti dalle callback
json_object *jresp = NULL;
json_object *jtresh = NULL;

sdepAddressList_f *addressList=NULL;
uint32_t attachedNum;
int isSignaled = 0;
int errorFlag = 0;

int sd; /* Il socket descriptor del client */
struct sockaddr_in server_addr; /* l'indirizzo del server */
struct hostent *hp;

int listenfd = 0, connfd = 0;
int SDEP_ERROR_CALLBACK_FLAG = 0;
/***************************************DATA SAMPLING DATA*************************************/

uint8_t data_oneshot[INDEX_ONESHOT];
uint8_t current_index_oneshot;
int numBuffers;
PeriodicBuffer buffers;
int numBuffers;
int dataArrivResponseSkipping;
/****************************************END************************************************/




/*******************************************DRIVER FUNCTIONS ********************************/

//SIGNAL handler

/* @brief Updates the attached probes list in case of SIGLSITCHANGE sig_num
 * 		  Sends threshold infos to the client on another socket.
 * */
void sig_handler(int sig_num){
	uint8_t pAddress;
	isSignaled = 1;
	int i=0;
	if (sig_num == SIGLISTCHANGE){
		printf("SIGNAL: You have received an ATTACHED LIST CHANGE signal.\n");
		freeAttachedList(addressList);
		getAttachedList(&addressList,&attachedNum);
		for(i=0;i<attachedNum;i++)
			printf("Probe [%d] with pAddress = %d and pSerial = %d.\n",i,addressList[i].pAddress,addressList[i].pSerial);
	}
	if (sig_num == SIGDEVINTERRUPT) {
		popNextInterrupted(&pAddress);
		printf("[LOG] SIGNAL: You have received an INTERRUPT signal from %x.\n", pAddress);

		if((sd = socket(AF_INET, SOCK_STREAM, 0)) < 0)
		{
			printf("[LOG] Errore nella creazione della socket\n");
			return;
		}

		if(connect(sd, (struct sockaddr *)&server_addr, sizeof(server_addr)) < 0)
		{
			printf("Errore di connessione al server\n");
			close(sd);
			return;
		}

		int status = sendSamplesRequestInt(pAddress);

		char *ret = assertCall(status);

		json_object *jobjstr = json_object_new_string(ret);
		if( jobjstr == NULL)
			printf("jobjstr is null\n");
		else json_object_object_add(jtresh,"return",jobjstr);

		if ( (send(sd, json_object_to_json_string(jtresh),strlen(json_object_to_json_string(jtresh)),0)) < 0 )
		{
			printf("[LOG] Send threshold failed!\n");
			close(sd);
			return;
		}

		close(sd);

		json_object_put(jtresh);
	}
	return;
}

/**************************************DRIVER CALLBACK IMPLEMENTATIONS************************************/
int8_t ackResponse(mAddress_t pAddress){
	printf("ACK received from Probe with pAddress = %d.\n",pAddress);
	return sdepOK;
}

int8_t dataArriveResponse(mAddress_t pAddress, sId_t sensorId, uint8_t samplesArray[], uint16_t arrSize){
	if(arrSize != 0)
	{
		data_oneshot[0] =   pAddress;
		data_oneshot[1] =  sensorId.sensorType;
		data_oneshot[2] =  sensorId.sensorNum;

		if(DATA_REQUEST_MODE == ONESHOTMode)
		{

			printf("Sono in oneshot ! \n");
			for(int i = 0; i < arrSize; i++)
			{
				data_oneshot[i+3] =  samplesArray[i]; //copia

				current_index_oneshot = i+4;
			}
		}else if(DATA_REQUEST_MODE == PERIODICMode)
		{
			if(dataArrivResponseSkipping > 0)
			{   printf("dataArriveResponseSkipping !\n");
				dataArrivResponseSkipping--;
				return sdepOK;
			}
#ifdef DEBUG

			printf("sono in periodic\n");
			printf("sensor pAddr: %d\n",pAddress);
			printf("sensor sID:num  %d  type: %d\n",sensorId.sensorNum, sensorId.sensorType);
			//Cerco il buffer associato al pAddr e al sid

#endif

			for(int i =0; i < numBuffers; i++)
			{
				printf("bi:%d buffer sensorNum sensorType %d %d\n",i,buffers[i].sid.sensorNum , buffers[i].sid.sensorType);

				if(pAddress== buffers[i].pAddr && sensorId.sensorNum == buffers[i].sid.sensorNum &&
						sensorId.sensorType == buffers[i].sid.sensorType)
				{
					printf("Sono il buffer %d\n",i);
					//Prendi il puntatore al relativo buffer;
					printf("ArrSize: %d\n\n\n",arrSize);
					//Riempio il buffer
					for(int j = 0; j < arrSize; j++)
					{
						if(i == 0)
							printf("current_idnex:%d\n",buffers[i].current_index);
						buffers[i].buffer[buffers[i].current_index] =  samplesArray[j];
						buffers[i].current_index++;
					}
				}
			}
			printf("[LOG] Data arrive callback!\n");
			printf("[LOG] Data arrived: \n");
			printf("[LOG] arrSize = %d\n",arrSize);
			int i = 0;
			for(i=0;i<arrSize;i++){
				printf("[LOG] samplesArray:");
				printf(" [%d]",samplesArray[i]);
			}


		}
		else printf("MI SONO PERSO\n");


	}
	return sdepOK;
}

int8_t errorResponse(mAddress_t pAddress, sError_t sErrorList[], uint16_t seNum){
	errorFlag = 1;
	printf("[LOG] Error Callback!\n");
	jresp = errList_Json(sErrorList, seNum);
	for(int i= 0; i < seNum; i++)
	{
		printf("Error :%d\n",sErrorList[i].errorCode);
	}
	return sdepOK;
}

int8_t capabilityResponse(mAddress_t pAddress, sCapability_t sCapsList[], uint16_t scNum){
	jresp = capList_Json(sCapsList, scNum);
	printf("[LOG] Capablity Callback!\n");
	return sdepOK;
}

int8_t customResponse(mAddress_t pAddress, mPayload_t *mPayload, pLength_t pLength){

	return sdepOK;
}

int8_t dataArriveResponseInt (mAddress_t pAddress, sId_t sensorId, uint8_t intType, uint8_t samplesArray[], uint16_t arrSize){
	int i = 0;

	jtresh = json_object_new_object();
	json_object *temp = json_object_new_object();
	json_object *arr = json_object_new_array();

	for(i = 0; i < arrSize; i++)
		json_object_array_add(arr,json_object_new_int(samplesArray[i]));

	json_object_object_add(temp, JSON_KEY_PADDR, json_object_new_int(pAddress));
	json_object_object_add(temp, JSON_KEY_SENSORID, sId_tJson(&sensorId));
	json_object_object_add(temp, JSON_KEY_INTTYPE, json_object_new_int(intType));
	json_object_object_add(temp,JSON_KEY_SAMPLESPACKET,arr);

	json_object_object_add(jtresh,JSON_KEY_OBJECT,temp);

	return sdepOK;
}

int8_t errorResponseInt (mAddress_t pAddress){

	return sdepOK;
}


/*************************************SERVER FUNCTIONS **************************************************/

void callSendDisconnect(mAddress_t pAddress)
{
	/* non ha senso chiamare la assertCall perchè
	 * questa funzione non restituisce alcun errore ma solo il
	 * restante numero di device collegati
	 * inoltre, il controllo sull'esistenza del device da scollegare è già fatto a monte
	 */
	forceDisconnect(pAddress);
}
/* @brief Calls the sendCapabilityRequest method on the probe with address pAddress and
 * adds to the global json object jresp the returned status code in form of string
 * @param pAddress: address of probe
 * */
void callSendCapabilityRequest(mAddress_t pAddress)
{
	json_object_object_add(jresp,"return",json_object_new_string(assertCall(sendCapabilityRequest(pAddress))));
}

/* @brief Calls the sendConfigTo method on the probe with address pAddress and
 * adds to the global json object jresp the returned status code in form of string
 * @param pAddress: address of probe
 * @param sConfList[]: list of configurations of sensors
 * @param scNum: size of sConfList[]
 * */
void callSendConfigTo(mAddress_t pAddress, sConfig_t sConfList[], uint16_t scNum)
{
	json_object_object_add(jresp,"return",json_object_new_string(assertCall(sendConfigTo(pAddress,sConfList,scNum))));
}
/* @brief Calls the sendSamplesRequest method on the sensor with sensor ID sid of probe with address pAddress and
 * adds to the global json object jresp the returned status code in form of string
 * @param pAddress: address of probe
 * @param sid: sensor id
 * */
void callSendSamplesRequest(mAddress_t pAddress, sId_t sid)
{
	json_object_object_add(jresp,"return",json_object_new_string(assertCall(sendSamplesRequest(pAddress,sid))));
}

/**
 * @brief: reads a string from socket
 * @param: connfd is the socket descriptor
 * @param: buf is the buffer that contains the whole string
 */
void readString(int connfd,char* buf)
{
	char temp[1000];
	bzero(temp,1000);
	ssize_t r;
	while(1)
	{
		r = recv(connfd, temp, 1000,0);
		if(temp[r-1] != '\n')
		{
		temp[r]='\0';
		strcat(buf,temp);
		}else {
			temp[r-1]='\0';
			strcat(buf,temp);
			break;
		}
	}
}

/* @brief initializes the server and returns its socket descriptor
 * @param listenfd: pointer to server socket descriptor
 */
void server_init(int* listenfd)
{
	struct sockaddr_in serv_addr;

	*listenfd = socket(AF_INET, SOCK_STREAM, 0);

	serv_addr.sin_family = AF_INET;
	serv_addr.sin_addr.s_addr = htonl(INADDR_ANY);
	serv_addr.sin_port = htons(PORT_SERVER);

	bind(*listenfd, (struct sockaddr*)&serv_addr, sizeof(serv_addr));
	printf("[LOG] Binding.\n");
}

/* @brief Sends the json string of the jresp json_object back to client
 * @param connfd: is the socket descriptor of client
 * @retval success or failure
 */
int sendJSONString(int connfd)
{
	char temp_buff[BUF_SZ];

	if (strncpy(temp_buff, json_object_to_json_string(jresp),BUF_SZ) == NULL)
	{
		printf("strcpy");
		return EXIT_FAILURE;
	}

	printf("[LOG] Stringa da mandare: %s.\n",temp_buff);

	if (send(connfd, temp_buff, strlen(temp_buff),0) == -1)
	{
		perror("write");
		return EXIT_FAILURE;
	}
	printf("Stringa mandata\n");
	return EXIT_SUCCESS;
}

/* @brief Returns in form of string the status code returned by the called function
 * @param status: the return value of the sdep library function
 * @retval Status code in string form
 */
char* assertCall(int status)
{
	if (errorFlag == 1)
	{
		errorFlag = 0;
		return "ERROR CALLBACK";
	}
	switch(status)
	{
	case -sdepERROR:
		return "ERROR";
		break;
	case -sdepBADMESSAGE:
		return "BADMESSAGE";
		break;
	case -sdepPAYTOOBIG:
		return "PAYTOOBIG";
		break;
	case -sdepNODEVADDR:
		return "NODEVADDR";
		break;
	case -sdepBADRESPONSE:
		return "BADRESPONSE";
		break;
	case sdepOK:
		return "sdepOK";
		break;
	default:
		return "NOT_KNOWN_ERROR";
		break;
	}
}

/* @brief Checks if the json_tokener_parse operation has been successful. If it isn't
 * 		  sends an error back to the client.
 * @param jobj: the json_object returned from json_tokener_parser
 * @param connfd: client socket descriptor
 * @retval -1 if failed, 0 if didn't.
 */
int checkParser(json_object* jobj, int connfd)
{
	if(jobj == NULL)
	{
		printf("eccezione nel parsing iniziale\n");
		ADD_RET(ERROR_JSON_PARSING);
		sendJSONString(connfd);
		return -1;
	}
	return 0;
}

/* @brief Checks if the probe address of the client requested operation exists
 * @param pAddress: the probe address of the client requested operation
 * @param addressList[]: the current probe list held locally
 * @param attachedNum: the number of the connected probes
 * @retval: 0 if it exists else -1
 */
int checkAddressExistence(uint8_t pAddress,sdepAddressList_f addressList[],uint8_t attachedNum)
{
	int i = 0;
	for(i = 0 ; i < attachedNum; i++)
	{
		if(pAddress==addressList[i].pAddress)
			return 0;
	}

	return -1;
}

/* @brief Finds the "Method" field inside the received json object and returns its value
 * if no "Method" field is found -1 is returned
 * @param jobj: received and parsed json object
 * @param method: found method
 * @retval -1 if no "Method" field is found
 * */
int findMethod(json_object *jobj,int *method)
{
	json_object *jTempM = json_object_new_object();
	jTempM = json_object_object_get(jobj,"methodName");
	if (jTempM == NULL)
	{
		printf("[LOG] ERROR: field Method not found!\n");

		return -1;
	}
	else{
		*method=json_object_get_int(jTempM);
		printf("[LOG] Method found: %d.\n",*method);
	}
	return 0;

}
/* @brief Finds the "pAddress" field inside the received json object and returns its value
 * if no "pAddress" field is found -1 is returned
 * @param jobj: received and parsed json object
 * @param pAddr: found pAddress
 * @retval -1 if no "pAddress" field is found
 * *///@rm
int findAddress(json_object *jobj, uint8_t *pAddr)
{
	json_object *jTemppA = json_object_new_object();
	jTemppA = json_object_object_get(jobj,"pAddress");
	if (jTemppA == NULL)
	{
		printf("[LOG] ERROR: field pAddress not found!\n");
		return -1;
	}

	else{
		*pAddr = json_object_get_int(jTemppA);
		printf("[LOG] pAddress found: %d.\n",*pAddr);
	}
	return 0;
}


int Periodic_elaboration(int connfd,uint8_t pAddress, sConfig_t *  C_list, uint16_t SLen, uint16_t scNum){
	int status = 0;
	struct timeval  tv1;
		printf("connfd ricevuta:%d\n",connfd);

		//Skippa i primi n campionamenti
		dataArrivResponseSkipping = scNum ;
		//Inizializza i	 buffers
		// Numero di buffers
		numBuffers= scNum;
		buffers = (PeriodicBuffer)malloc(sizeof(OnePeriodicBuffer)*scNum);
//		for(int i = 0; i < scNum; i++)
//			buffers[i] = malloc(sizeof(PeriodicBuffer));

		/*** ** Inizializzo le strutture buffer ****/
		for(int i = 0 ; i < scNum; i++)
		{
			buffers[i].pAddr = 0;
			buffers[i].sid.sensorNum = C_list[i].sensorId.sensorNum;
			buffers[i].sid.sensorType = C_list[i].sensorId.sensorType;
			buffers[i].current_index = 0;

			printf("bi:%d sensorNum sensorType %d %d\n",i,buffers[i].sid.sensorNum , buffers[i].sid.sensorType);

		}


		/*************** Salta primo campionamento per svuotare il buffer stm ******************/
		for(int i=0; i<scNum; i++)
		{
			status =sendSamplesRequest(pAddress, C_list[i].sensorId);
			if (status != 0)
			{
				printf("Error status:%d\n");
				//ADD_RET("PERIODIC ERROR");
				//sendJSONString(connfd);
			//	return -1;
			}
		}




			if (gettimeofday(&tv1, NULL)==-1)
			{    printf("Errore start time \n");
				return -1;
			}
			//Avvio il timer
			long int startTime =  tv1.tv_sec;
			long int currentTime = tv1.tv_sec - startTime;

//		status = anodine_timeManager_startTimer();
//		if (status != 0)
//			return TIMER_START_ERROR;


		// Finchè minore di sampleLen
		while(currentTime < SLen)
		{

			//Se non sono arrivati degli errori dormi , altrimenti ricampiona
			if(!SDEP_ERROR_CALLBACK_FLAG)
			{//Tempo di attesa per riempimento del buffer
				sleep(1);
				//Resetta il flag
				SDEP_ERROR_CALLBACK_FLAG = 0;
			}

			for(int i=0; i<scNum; i++)
			{
				status =sendSamplesRequest(pAddress, C_list[i].sensorId);
					if (status != 0)
					{
						printf("Error status:%d\n");
						//ADD_RET("PERIODIC ERROR");
					//	sendJSONString(connfd);
						//return -1;
					}
			}

				//Aggiorna current time
				if (gettimeofday(&tv1, NULL)==-1)
				{
						printf("Errore current time \n");
						ADD_RET("PERIODIC ERROR");
						sendJSONString(connfd);
						return -1;
				}
				currentTime = tv1.tv_sec - startTime;
				printf("current time :%d\n",currentTime);
		//	time = anodine_timeManager_getTimer(SEC);
		//	printf("time current:%ld\n",currentTime);

		}
#ifdef DEBUG

		printf("numSC:%d\n",scNum);
		for(int i = 0; i < scNum; i++)
		{

				printf("buffer%d:, currentIndex:%d\n",i,buffers[i].current_index);
				for(int j = 0; j < buffers[i].current_index; j++)
					printf("%d ",buffers[i].buffer[j]);
				printf("\n");
		}

#endif

		//crea il json array
		json_object *arr = json_object_new_array();
		for(int i = 0 ; i< scNum; i++)
		{
			// Crea l'exchange data
			sExchange_t exchangeData ;
			exchangeData.samplesPacket = buffers[i].buffer;
			exchangeData.sensorId = C_list[i].sensorId;
			json_object *obj_data = sExchange_tJson(&exchangeData,buffers[i].current_index);

			//aggiungi all'array
			json_object_array_add(arr,obj_data);
//			send(connfd,buffers[i].buffer,buffers[i].current_index,0);
		}
		//sSetta il ritorno e l'array con chiave object
		json_object_object_add(jresp,"return",json_object_new_string("sdepOK"));
		json_object_object_add(jresp, JSON_KEY_OBJECT,arr);

		sendDataJSONString(connfd);
		//Clean memory
		PeriodicEnd();
		return 0;

}
int PeriodicEnd()
{
	printf("Libera i buffer");
//	for(int i = 0; i < numBuffers; i++)
//		free(buffers[i]);
	free(buffers);
	return 1;
}
int sendDataJSONString(int connfd)
{
	char temp_buff[BUF_DATA_SZ];

	if (strncpy(temp_buff, json_object_to_json_string(jresp),BUF_DATA_SZ) == NULL)
	{
		printf("strcpy");
		return EXIT_FAILURE;
	}

	printf("[LOG] Stringa da mandare: %s.\n",temp_buff);

	if (send(connfd, temp_buff, strlen(temp_buff),0) == -1)
	{
		perror("write");
		return EXIT_FAILURE;
	}
	printf("Stringa mandata\n");
	return EXIT_SUCCESS;
}
/***************************************************END*********************************************/


/*********************************************STUB FUNCTIONS****************************************/
void testAttached()
{
	sdepAddressList_f addr_list[2];
	addr_list[0].pAddress = 1;
	addr_list[0].pSerial = 11;
	addr_list[1].pAddress = 2;
	addr_list[1].pSerial = 21;

	printf("sono qui\n");
	//json_object* j= addrList_Json(addr_list, 2);
	jresp = addrList_Json(addr_list, 2);

	//jresp= json_object_object_get(j, "AddressList");
}
void testCapability()
{

	jresp = json_object_new_object();
	sCapability_t a[2];
	mAddress_t pAddress=0;
	uint8_t sensorCap;

	sensorCap=1;
	a[0].sensorCap.sampleLen = (sensorCap & 0b11110000) >> 4;
	a[0].sensorCap.reqSupport = (sensorCap & 0b00001100) >> 2;
	a[0].sensorCap.reserved = (sensorCap & 0b00000010) >> 1;
	//a[0].sensorCap.reqInterrupt = (sensorCap & 0b00000001);
	a[0].sensorQuantity = 2;
	a[0].sensorType = 3;

	sensorCap=15;
	a[1].sensorCap.sampleLen = (sensorCap & 0b11110000) >> 4;
	a[1].sensorCap.reqSupport = (sensorCap & 0b00001100) >> 2;
	a[1].sensorCap.reserved = (sensorCap & 0b00000010) >> 1;
	//a[1].sensorCap.reqInterrupt = (sensorCap & 0b00000001);
	a[1].sensorQuantity = 5;
	a[1].sensorType = 6;

	//json_object* j= capList_Json(a, 2);
	jresp = capList_Json(a, 2);
	//jresp= json_object_object_get(j, "CapabilityList");
	//printf("jresp: %s (line %d)",json_object_get_string(jresp), __LINE__);
	//capabilityResponse (pAddress, a, 2);
	//sendCapabilityRequest(pAddress);
}

void testDisconnect(uint8_t pAddress)
{
	printf("disconnessione della probe %d\n", pAddress);
}
void testDataSend(json_object* obj)
{
	jresp = json_object_new_object();

	json_object* o  = json_object_object_get(obj,"variableField");

	json_object* ogg = json_tokener_parse(json_object_get_string(o));

	sExchange_t exchange;
	int sensorNum = json_object_get_int(json_object_object_get(ogg,"sensorNum"));
	int sensorType = json_object_get_int(json_object_object_get(ogg,"sensorType"));

	printf("sensT %d, sensNu %d\n", sensorType, sensorNum);

	sId_t sensorID= { .sensorType = sensorType, .sensorNum=sensorNum};
	uint8_t samples[] = {1,2,3,4};

	exchange.samplesPacket = samples;
	exchange.sensorId = sensorID;


	printf("obj: %s\n",json_object_get_string(ogg));
	jresp = sExchange_tJson(&exchange, 4); //vuole anche la dimensione dell'array
	printf("ogg dataSend: %s\n", json_object_get_string(jresp));

	//jresp = json_object_array_add()
}
void testConfig(json_object* jobj)
{
	jresp = json_object_new_object();

	int scNum = json_object_get_int(json_object_object_get(jobj,"variableField"));
	printf("scNum %d\n", scNum);
	sConfig_t *confList = (sConfig_t*)malloc(sizeof(sConfig_t)*scNum);

	json_object* papa = json_object_object_get(jobj,"object");
	//	printf("papa:%s",json_object_get_string(papa));
	json_object * mamma = json_tokener_parse(json_object_get_string(papa));

	int status=jSon2sConfig_tList(mamma, confList);
	printf("status %d\n", status);

	//	int i=0;
	//	for(i=0; i<scNum; i++){
	//		printf("sensNum %d, sensType %d, sensMode%d",
	//				confList[i].sensorId.sensorNum, confList[i].sensorId.sensorType,
	//				confList[i].sensorMode);
	//	}

	//verificare
	//	free(confList);
}


uint8_t server_extractObjectField(json_object* jobj, json_object* object, json_object* variableObject)
{

#ifdef DEBUG
			printf("[LOG] Malloc of configList done.\n");
#endif

			json_object *temp = json_object_object_get(jobj,"object");

#ifdef DEBUG
			printf("[LOG] configList json: %s.\n",json_object_get_string(temp));
			printf("[LOG] Parsing...\n");
#endif
			object = json_tokener_parse(json_object_get_string(temp));

#ifdef DEBUG
			printf("[LOG] Parsed...\n");
#endif

			variableObject = json_object_object_get(jobj,JSON_KEY_VARIABLE_FIELD);
			printf("Todo controlli sulle null");
			if(variableObject == NULL)
				return -1;
			printf("variable object non è null:\n");
			return checkParser(object,connfd);

}

