/*
 * anodine-timeManager.c
 *
 *  Created on: 17 lug 2016
 *      Author: anodine
 */

#include "anodine-timeManager.h"

int anodineSleep(timeUnit unit, long int timeValue) {

	struct timespec nanos;

	switch(unit) {
		case SEC:
			return sleep(timeValue);
		case MILLI:
			//convert timeValue [ms] in nanoseconds
			nanos.tv_nsec = (timeValue%1000)*1000000;
			nanos.tv_sec = timeValue/1000;
			return nanosleep(&nanos,NULL);
		case MICRO:
			//convert timeValue [us] in nanoseconds
			nanos.tv_nsec = (timeValue%1000000)*1000;
			nanos.tv_sec = timeValue/1000000;
			return nanosleep(&nanos,NULL);
		case NANO:
			nanos.tv_nsec = timeValue%1000000000;
			nanos.tv_sec = timeValue/1000000000;
			return nanosleep(&nanos,NULL);
		default:
			return -1;
	}

}

int anodine_timeManager_startTimer() {
	struct timeval  tv1;
	if (gettimeofday(&tv1, NULL)==-1)
		return -1;

	//take the time in us
	start_time = current_time = tv1.tv_sec*1000000 + tv1.tv_usec;
	printf("anodine startTime:%ld\n",start_time);

	return 0;
}

long int anodine_timeManager_getTimer(timeUnit unit) {
	struct timeval  tv1;

	if (gettimeofday(&tv1, NULL) == -1)
		return -1;

	switch(unit) {
		case SEC:
			current_time = tv1.tv_sec + tv1.tv_usec/1000000;
			printf("anodine current_time:%ld\n",current_time);
			return current_time - start_time/1000000;
		case MILLI:
			current_time = tv1.tv_sec*1000 + tv1.tv_usec/1000;
			return current_time - start_time/1000;
		case MICRO:
			current_time = tv1.tv_sec*1000000 + tv1.tv_usec;
			return current_time - start_time;
		case NANO:
			current_time = tv1.tv_sec*1000000000 + tv1.tv_usec*1000;
			return current_time - start_time*1000;
		default:
			return -1;
	}

}
