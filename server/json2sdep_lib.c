/*
 * json2sdep_lib.c
 *
 *  Created on: 04 lug 2016
 *      Author: anodine
 */

#include "json2sdep_lib.h"
#include "server_f.h"
/**
 * @brief Converts a JSON object into a sCapability_t struct
 * @param j - pointer to the JSON object to convert
 * @param capability - output parameter that represents a sCapability_t struct
 * @retval function's outcome (negativeValues == ERROR)
 */
int jSon2sCapability_t(json_object *j, sCapability_t *capability)
{
	if ( json_object_object_length(j) != 3 )
		return FRM_ERR; //ERROR: unexpected format for sCapability_t struct

	json_object *jTemp = json_object_object_get(j,JSON_KEY_SENSOR_TYPE);

	if (jTemp == NULL)
		return NULL_ERR; //ERROR: field not found!

	capability->sensorType = json_object_get_int(jTemp);

	jTemp = json_object_object_get(j,JSON_KEY_SENSOR_QUANTITY);

	if (jTemp == NULL)
		return NULL_ERR;

	capability->sensorQuantity = json_object_get_int(jTemp);

	jTemp = json_object_object_get(j,JSON_KEY_SENSOR_CAP);

	if (jTemp == NULL)
		return NULL_ERR;

	uint8_t sensorCap = json_object_get_int(jTemp);

	capability->sensorCap.sampleLen = (sensorCap & 0b11110000) >> 4;
	capability->sensorCap.reqSupport = (sensorCap & 0b00001100) >> 2;
	capability->sensorCap.reserved = (sensorCap & 0b00000010) >> 1;
	capability->sensorCap.intSupport = (sensorCap & 0b00000001);

	//capability->sampleLen = json_object_get_int(jTemp);

	return OK;
}

/**
 * @brief Converts a JSON object into a sId_t struct
 * @param j - pointer to the JSON object to convert
 * @param sensorId - output parameter that represents a sId_t struct
 * @retval function's outcome (negativeValues == ERROR)
 */
int jSon2sId_t(json_object *j, sId_t *sensorId)
{
	if ( json_object_object_length(j) != 2 )
		return FRM_ERR; //ERROR: unexpected format for sId_t struct

	json_object *jTemp = json_object_object_get(j,JSON_KEY_SENSOR_TYPE);

	if (jTemp == NULL)
		return NULL_ERR; //ERROR: field not found!

	sensorId->sensorType = json_object_get_int(jTemp);

	jTemp = json_object_object_get(j,JSON_KEY_SENSOR_NUM);

	if (jTemp == NULL)
		return NULL_ERR;

	sensorId->sensorNum = json_object_get_int(jTemp);

	return OK;
}


/**
 * @brief Converts a JSON object into an array of sCapability_t struct
 * @param j - pointer to the JSON object of type json_array_type
 * @param capList - output parameter that represents an sCapability_t struct array
 * @retval function's outcome (negativeValues == ERROR)
 */
int jSon2sCapability_tList(json_object *j, sCapability_t *capList)
{
	int status;
	json_object *jTemp;

	int i=0;
	while ( i < json_object_array_length(j) ) {
		jTemp = json_object_array_get_idx(j,i);

		status = jSon2sCapability_t(jTemp,&capList[i]);
		if (status != OK)
			return status;

		i++;
	}

	return OK;
}

/**
 * @brief Converts a JSON object into a sdepAddressList_f struct
 * @param j - pointer to the JSON object to convert
 * @param sensorId - output parameter that represents a sdepAddressList_f struct
 * @retval function's outcome (negativeValues == ERROR)
 */
int jSon2sdepAddressList_f(json_object *j, sdepAddressList_f *addr)
{
	if ( json_object_object_length(j) != 2 )
		return FRM_ERR; //ERROR: unexpected format for sdepAddressList_f struct

	json_object *jTemp = json_object_object_get(j,JSON_KEY_PADDR);

	if (jTemp == NULL)
		return NULL_ERR; //ERROR: field not found!

	addr->pAddress = json_object_get_int(jTemp);

	jTemp = json_object_object_get(j,"pSerial");

	if (jTemp == NULL)
		return NULL_ERR;

	addr->pSerial = json_object_get_int(jTemp);

	return OK;
}

/**
 * @brief Converts a JSON object into an array of sdepAddressList_f struct
 * @param j - pointer to the JSON object of type json_array_type
 * @param capList - output parameter that represents an sdepAddressList_f struct array
 * @retval function's outcome (negativeValues == ERROR)
 */
int jSon2sdepAddressList_fList(json_object *j, sdepAddressList_f *addrList)
{
	int status;
	json_object *jTemp;

	int i=0;
	while ( i < json_object_array_length(j) ) {
		jTemp = json_object_array_get_idx(j,i);

		status = jSon2sdepAddressList_f(jTemp,&addrList[i]);
		if (status != OK)
			return status;

		i++;
	}

	return OK;
}

/**
 * @brief Converts a JSON object into a sConfig_t struct
 * @param j - pointer to the JSON object to convert
 * @param sConfig_t - output parameter that represents a sConfig_t struct
 * @retval function's outcome (-1 == ERROR)
 */
int jSon2sConfig_t(json_object *j, sConfig_t *config)
{
	if ( json_object_object_length(j) != 3 )
		return FRM_ERR; //ERROR: unexpected format for sConfig_t struct

	json_object *jTemp = json_object_object_get(j,"sensorId");

	if (jTemp == NULL)
		return NULL_ERR; //ERROR: field not found!

	if(jSon2sId_t(jTemp,&config->sensorId) != OK)
		return jSon2sId_t(jTemp,&config->sensorId);

	jTemp = json_object_object_get(j,JSON_KEY_SENSOR_MODE);

	if (jTemp == NULL)
		return NULL_ERR;

	uint8_t sensorMode = json_object_get_int(jTemp);

	config->sensorMode.sActive = (sensorMode & 0b10000000) >> 7;
	config->sensorMode.reqSettings = (sensorMode & 0b01100000) >> 5;
	config->sensorMode.reserved = (sensorMode & 0b00011111);

	jTemp = json_object_object_get(j,"sensorConfig");

	if (jTemp == NULL)
		return NULL_ERR;

	config->sensorConfig = json_object_get_int(jTemp);

	return 0;
}

/**
 * @brief Converts a JSON object into an array of sConfig_t struct
 * @param j - pointer to the JSON object of type json_array_type
 * @param configList - output parameter that represents an sConfig_t struct array
 * @retval function's outcome (negativeValues == ERROR)
 */
int jSon2sConfig_tList(json_object *j, sConfig_t *configList) {

	int status;
	json_object *jTemp;

	int i=0;
	while ( i < json_object_array_length(j) ) {
		jTemp = json_object_array_get_idx(j,i);
		status = jSon2sConfig_t(jTemp,&(configList[i]));
		if (status != OK)
			return status;

		i++;
	}

	return OK;

}

/**
 * @brief Converts a JSON object into a sError_t struct
 * @param j - pointer to the JSON object to convert
 * @param err - output parameter that represents a sError_t struct
 * @retval function's outcome (-1 == ERROR)
 */
int jSon2sError_t(json_object *j, sError_t *err) {

	if ( json_object_object_length(j) != 2 )
		return FRM_ERR; //ERROR: unexpected format for sError_t struct

	json_object *jTemp = json_object_object_get(j,"sensorId");

	if (jTemp == NULL)
		return NULL_ERR; //ERROR: field not found!

	if(jSon2sId_t(jTemp,&err->sensorId) != OK)
		return jSon2sId_t(jTemp,&err->sensorId);

	jTemp = json_object_object_get(j,"errorCode");

	if (jTemp == NULL)
		return NULL_ERR;

	err->errorCode = json_object_get_int(jTemp);

	return 0;

}

/**
 * @brief Converts a JSON object into an array of sError_t struct
 * @param j - pointer to the JSON object of type json_array_type
 * @param errList - output parameter that represents an sError_t struct array
 * @retval function's outcome (negativeValues == ERROR)
 */
int jSon2sError_tList(json_object *j, sError_t *errList) {

	int status;
	json_object *jTemp;

	int i=0;
	while ( i < json_object_array_length(j) ) {
		jTemp = json_object_array_get_idx(j,i);

		status = jSon2sError_t(jTemp,&errList[i]);
		if (status != OK)
			return status;

		i++;
	}

	return OK;
}
