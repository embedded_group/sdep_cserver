/**
* University of Naples Federico II
* Master of Embedded System
*
* Academic Year: 2015-2016
*
* Group Number: 2
* Engineers: Castello Oscar
*            Iorio Raffaele
*/

#ifndef SDEP_PHYCTL_H
#define SDEP_PHYCTL_H

#include <fcntl.h>
#include <stdint.h>
#include <stdlib.h>
#include <string.h>
#include <signal.h>
#include <sys/errno.h>
#include <sys/ioctl.h>

#define DEV_NAME "/dev/sdepUSB"

/* Define the return codes */
#define SUCCESS 	0x00
#define LISTISFULL 	0x01
#define NOSDEPDEV	0x02

/* Define the signal macro */
#define SIGLISTCHANGE	SIGUSR1
#define SIGDEVINTERRUPT SIGUSR2

/* SDEP Versioning macro */
#define SDEPVER_mj(sdepmv)		((sdepmv & 0xFF000000) >> 24) // SDEP version major number
#define SDEPVER_mn(sdepmv)		((sdepmv & 0x00FF0000) >> 16) // SDEP version minor number
#define SDEPVER_sub(sdepmv)		((sdepmv & 0x0000FF00) >> 8)  // SDEP version subversion number
#define SDEPVER_rel(sdepmv)		((sdepmv & 0x000000FF) >> 0)  // SDEP version release number

/* Define of the IOCTL message */
#define IOCTL_MAGIC      'u'
// Use this iotctl command for get a quantity of attached device
#define IOCTL_HOWMANYDEVICE  _IO(IOCTL_MAGIC, 0x50) // With no parameters
// Use this iotctl command for get a list of attached device
#define IOCTL_GETLIST        _IOW(IOCTL_MAGIC, 0x51, char*) // From kernel module to user process
// Use this iotctl command for select the device where read and write the message
#define IOCTL_SELECT         _IOR(IOCTL_MAGIC, 0x52, int) // From user process to kernel module
// Use this iotctl command for get the interrupted table (16 byte)
#define IOCTL_POPNEXTINT     _IOW(IOCTL_MAGIC, 0x53, char*) // From user process to kernel module
// Use this iotctl command for forced physical disconnect of a probe
#define IOCTL_DISCONNECT     _IOR(IOCTL_MAGIC, 0x54, int) // From user process to kernel module

typedef struct {
	uint8_t pAddress;
    uint32_t pSDEPVer;	
	uint32_t pSerial;
} sdepAddressList_f;

int32_t openDriver();
int32_t closeDriver();

int32_t registerSignalHandler(int sig_num, void (*sig_handler)(int sig_num));
int32_t popNextInterrupted(uint8_t *pAddress);

int32_t getAttachedList(sdepAddressList_f **attachedList, uint32_t *attachedNum);
int32_t freeAttachedList(sdepAddressList_f *attachedList);

int32_t forceDisconnect(uint8_t pAddress);

// Physical callbacks
int32_t selectAddress(uint8_t pAddress);
int32_t writeChars(const uint8_t *data, uint16_t length);
int32_t readChars(uint8_t *data, uint16_t length);
#endif //SDEP_PHYCTL_H
